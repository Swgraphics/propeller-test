'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
// var cleanCSS = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var livereload = require('gulp-livereload');
var jshint = require('gulp-jshint');
var sourcemaps = require('gulp-sourcemaps');
//var gutil = require('gulp-util');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
// var wpRev = require('gulp-wp-rev');
var replace = require('gulp-replace');
var runSequence = require('run-sequence');

var DEST = '../';

var livereloadOptions = {
  host: '127.0.0.1', //important!
  reloadPage: 'index.php'
};

var production = false;
gulp.task('setenv', function() {
    production = true;
    return true;
});


//css
gulp.task('minifycss', function() {
  return gulp.src([
    // 'vendor/iLightbox/css/ilightbox.css',
    //'vendor/iLightbox/flat-dark-skin/skin.css',
    'scss/main.scss',
    ])
      .pipe(sourcemaps.init())
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(concat('style.css'))
      .pipe(autoprefixer({
        browsers: ['last 2 versions','iOS > 7'], // readme: https://github.com/postcss/autoprefixer#options
        cascade: false
      }))
      .pipe(rename({ extname: '.min.css' }))
      .pipe(sourcemaps.write('/maps'))
      .pipe(gulp.dest(DEST+'/css'))
      .pipe(livereload());
});


//js
gulp.task('jshint', function() {
  return gulp.src('js/*.*')
      .pipe(jshint());
});

gulp.task('minifyjs', function() {
  
  var dropConsole = false;

  if(production) dropConsole = true;

  return gulp.src([
    // main js
    'js/scripts.js',



    ])
      .pipe(sourcemaps.init())
      .pipe(concat('script.js'))
      .pipe(uglify({compress: {drop_console: dropConsole}}))
      .pipe(rename({ extname: '.min.js' }))
      .pipe(sourcemaps.write('/maps'))
      .pipe(gulp.dest(DEST+'/js'))
      .pipe(livereload());
});

//images
gulp.task('imagemin', function() {
  return gulp.src('img/**.*')
        .pipe(imagemin())
        .pipe(gulp.dest(DEST+'/img'))
        .pipe(livereload());
});

// revision the WordPress enqueued scripts and styles

// css task
gulp.task('css', function(callback) {
  runSequence('minifycss',
              // 'rev',
              callback);
});

// javascript task
gulp.task('js', function(callback) {
  runSequence('minifyjs',
              // 'rev',
              callback);
});

// image optimisation task
gulp.task('img', ['imagemin']);


// production build task (removes console logs)
gulp.task('build', function(callback) {
  runSequence('setenv',
              ['css','js','img'],
              callback);
});

// default task
gulp.task('default', function(callback) {
  runSequence(['css','js','img'],
              // 'rev',
              callback);
});


// watch / live reload
gulp.task('watch', ['default'], function() {
  livereload.listen(livereloadOptions);
  gulp.watch('scss/**/**.*', ['css']);
  gulp.watch('js/**/**.*', ['js']);
  gulp.watch('img/**/**.*', ['img']);
});


