$(document).ready(function() {
    //Get external file
    $.getJSON('http://design.propcom.co.uk/buildtest/accordion-data.json', function(data) {

        console.log( data);
        $.each(data.blocks, function(i,meal) {
            var output= '';
                output+= '<div class="accordion">' + data.blocks[i].heading + '</div>';
                output+= '<div class="panel">' + data.blocks[i].content + '</div>';
            $("#accordion_wrapper").append(output);
        })
        
        var allPanels = $('.panel').hide();
        $('.accordion').click(function(){
            allPanels.slideUp();
            $('.accordion').removeClass('active');
            $(this).addClass('active').next().slideDown('panel');
            return false;
        })
    });
});



